#!/usr/bin/env bash

source .env

rm -rf "${WORKING_DIR}/vendor"

docker exec -i $(docker-compose ps -q php) sh -c 'cd /www && composer require "twig/twig:^2.0"'
