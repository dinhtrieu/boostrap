<?php
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('template');
$twig = new Twig_Environment($loader);
$twig->getExtension('Twig_Extension_Core')->setTimezone('Asia/Ho_Chi_Minh');


echo $twig->render("index.html", array());


?>
