# Overview
Docker containers: Nginx, PHP (fpm), and MariaDB.


* Start docker containers *

`./init.sh`

* Destroy containers and image

`./destroy.sh`

* Installing **twig**

`./install-twig.sh`
