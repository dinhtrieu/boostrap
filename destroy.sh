#!/usr/bin/env bash

# Shutdown and remove all containers
docker-compose down

# Remove custom php image
source .env
docker rmi ${PHP_IMAGE_NAME}
