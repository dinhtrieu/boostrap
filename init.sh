#!/usr/bin/env bash

if [ ! -f .env ]; then
  cp .env-dist .env
fi

source .env
docker-compose up -d
